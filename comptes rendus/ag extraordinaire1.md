# Assemblée générale extraordinaire

Le 18 octobre 2016 à 18h15, l’ensemble des actionnaires s’est retrouvé spontanément permettant — conformément aux status — de procéder à une assemblée générale extraordinaire.

## Délibrations soumises au vote de l’assemblée

### Vote n°1 : une personne une voix

Commit : 5773db7e

* Pour : 3
* Contre : 0
* Abstention : 0

### Vote n°2 : capital principalement détenu par les employés

Commit : 84133186

* Pour : 3
* Contre : 0
* Abstention : 0

### Vote n°3 : l’entreprise rachète les actions au bout de cinq ans

Commit : a6b9472c

* Pour : 3
* Contre : 0
* Absention : 0

## Autres sujets

### Choix du logo
Ok pour la proposition (épingle à nourrice dans le clavier). On va payer les images noun project

À réfléchir: la police pour le texte pour aller avec

### Ouverture d’un PEE
Les placements sont volontaires pour chaque employé. L’entreprise peut abonder à hauteur de 300%
et 3000€/an. Les abondements ne sont pas soumis à l’impôt sur le revenu.

L’argent est bloqué pendant cinq ans.

La question principale est : comment est placé l’argent (éthique ?). Le label finansol est
probablement un peu léger.

### Explication de la comptabilité
Un fichier ODS sur owncloud pour la compta individuelle.

Zefyr pour la compta légale.

Possibilité de faire des factures séparément pour des rasons de confidentialité.

### Ouverture d’un compte IbanFirst
Ils proposent de faire proxy mais avec un compte à notre nom pour encaisser les paiements étrangers.
Cela peut permettre d’économiser plusieurs dizaines d’euros par mois.

### Accès au serveur

Résumé du serveur
* Debian avec chiffrement complet du disque
* Aucun service sur l’hôte
* Chaque service tourne sur un conteneur LXC
 * Résolveur DNS local
 * Tor : évite de faire du port forwarding
 * Base LDAP
 * owncloud
 * frontalweb : tls avec letsencrypt. Selon domain et chemin c’est redirigé vers le bon service

Kheops va écrire une doc pour la création d’images LXC pour que chacun puisse héberger ses propres besoins.

### Système de chat et autres services
* Doc technique : pour l’instant dans markdown dans gitlab
* Chat : XMPP auto-hébergé

### Futures recrues
* Peut-etre une personne debut 2017, dans l'hebergement web/secu

19h17 : la scéance est close.
