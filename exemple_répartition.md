# Exemple de répartition de revenus.

Jean apporte et gère un projet qui est réalisé à par Michel. Ce projet est
facturé 20 jours à 500 €. L’enveloppe est donc de 10 000 €, réalisé en janvier.

Jean et Michel se sont mis d’accord pour rémunérer l’apport de projet à
hauteur de 5%.

* 500 € sont destinés à Jean pour avoir apporté le projet ;
* 500 € sont destinés à couvrir la gestion de l’entreprise ;
* 500 € alimentent le compte de solidarité ;
* 500 € servent à rembourser divers frais liés au projet.

Au final, 8 000 € sont destinés au salaire de Michel répartis :
* 2 400 € de charges patronales ;
* 1 200 € de charges salariales ;
* 4 400 € de salaire net sur le compte en banque de Michel.

Le salaire étant répartir sur janvier, février et mars, le revenu net sera
de 1 467 € par mois (1 000 € de salaire fixe et 467 € de primes).

Si Michel peut choisir de ne pas travailler en février et mars. Cependant,
s’il décide de travailler à nouveau en mars, avec exactement la même facturation,
son salaire sera alors doublé en mars.

Dans l’hypothèse d’une facturation récurrente, le salaire du mois de janvier
sera de 1 467 €, celui de février de 2 934 € et tous les mois suivants de 4 401 €.

